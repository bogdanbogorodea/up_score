class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true 
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!

  def configure_permitted_parameters
    fields_form = [:name, :email, :new_password, :new_password_confirmation, :current_password]
    devise_parameter_sanitizer.permit(:account_update, keys: fields_form)
  end

end
