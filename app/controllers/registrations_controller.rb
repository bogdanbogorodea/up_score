class RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:user).permit(:name, :email, :password)
  end

  protected

  def after_sign_up_path_for(resource)
    "/" # <- Path you want to redirect the user to.
  end

  def update_resource(resource, params)
    return super if params["password"]&.present?
    resource.update_without_password(params.except("current_password"))
  end

end
