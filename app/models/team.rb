class Team < ApplicationRecord
  before_save :associate_team_statistic

  has_and_belongs_to_many :users
  has_and_belongs_to_many :games
  has_one :team_statistic, dependent: :destroy

  validate :exact_two_users,
  	:distinct_users
  	# :order_of_users_does_not_matter
  # should I add
  # validate :unique
  # or should I write unique_by_team_members validation?
	
  def associate_team_statistic
    self.team_statistic = TeamStatistic.get_new if self.team_statistic.nil?
  end

	# validations
		def exact_two_users
			if users.size != 2
				errors.add(:users, "not two users in team")
			end
		end

		def distinct_users
			user1 = users.first
			user2 = users.second
			if users.size == 2 and user1 == user2
				errors.add(:users, "team cannot have same user twice")
			end
		end
  

  # first implemented for database seeding
		def self.new_team(team_user1, team_user2)
			new_team = Team.new team_statistic: TeamStatistic.get_new
			new_team.users << team_user1 << team_user2
			new_team
		end

	def self.get_existing_or_new_team(user1, user2, all_teams)
		all_teams.each do |team|
			users = team.users
			return team if users.include? user1 and users.include? user2
		end
		Team.new_team(user1, user2)
	end
end
