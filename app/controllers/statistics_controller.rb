# TRY: give only 5 records to each ranking partial

class StatisticsController < ApplicationController
  before_action :set_user, only: [:all]

  @@user_not_in_database = :"Error: The given name does not match any registered user"
  @@selected_user_is_current_user = :"Error: Selected user must not be the current user"

  def own
    @user = current_user
  end

  def all
    # PROBLEMS
      # HOW TO HANDLE RANKINGS WHEN MORE TEAMS AND/OR USERS? Pagination? 3-7 items/page?
        # to display at once a great number of teams takes time
      # Efficiency
        # for 4 users, ~=200 games, ~=400 teams and ~=2000 goals- I must come back.
        # It should work well enough for 10 000 games, 20 000 teams, 200 users,
        # 150 000 users
        # Let's see if I could make it...
  end

  private
    def set_user
      @user_name_not_received = false
      @user_name_errors = false
      @user_name_error_messages = []

      user_name = params[:name]
      if user_name.nil?
        @user_name_not_received = true
      else
        set_errors user_name
      end
    end

    # how to write it better? Is this place good for this method?
    def set_errors(user_name)
      if current_user.name == user_name
        @user_name_errors = true
        @user_name_error_messages << @@selected_user_is_current_user
      end
      
      @user = User.find_by name: user_name
      if @user.nil?
        @user_name_errors = true
        @user_name_error_messages << @@user_not_in_database
      end 
    end

    def user_params # I must come back here to understand
      params.permit(:name)
    end
end