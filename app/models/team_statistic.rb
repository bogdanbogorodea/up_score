class TeamStatistic < ApplicationRecord
  belongs_to :team

  # a new class to Statistic to refactor this code?
  def update_with(won, goals_scored, own_goals_scored, duration)
		wins_no = self.wins_no
		wins_no += 1 if won
		games_played_no = 1 + self.games_played_no
		goals_scored_no = goals_scored + self.goals_scored_no
		own_goals_scored_no = own_goals_scored + self.own_goals_scored_no
		time_played = duration + self.time_played

		self.update wins_no: wins_no,
			games_played_no: games_played_no,
			goals_scored_no: goals_scored_no,
			own_goals_scored_no: own_goals_scored_no,
			time_played: time_played
	end

	def self.get_new
		self.new wins_no: 0,
      games_played_no: 0,
      goals_scored_no: 0,
      own_goals_scored_no: 0,
      time_played: 0
	end
end
