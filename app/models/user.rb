class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable#,
         #:confirmable
  before_save :associate_user_statistic


  include Gravtastic
  gravtastic



  validates_presence_of :name
  validates :name, uniqueness: true

  has_and_belongs_to_many :teams
  has_many :games, through: :teams
  has_one :user_statistic, dependent: :destroy

  
  def associate_user_statistic
    self.user_statistic = UserStatistic.get_new if self.user_statistic.nil?
  end

  # most frequent team members - COME BACK FOR EFFICIENCY
    def most_frequent_team_members(n, teams)
      co_user_frequency = self.co_user_to_frequency teams
      co_user_frequency.sort_by { |k, value| value }.reverse.take(n).to_h
    end

    def co_user_to_frequency(teams)
      co_user_to_frequency = Hash.new(0)
      teams.each do |team|
        team.users.each do |team_user|
          co_user_to_frequency[team_user] +=
            team.team_statistic.games_played_no if team_user != self
        end
      end
      co_user_to_frequency
    end

  # first implemented for database seeding
    def self.create_with(name_email, password)
      name_email.each do |name, email|
        User.create! email: email, name: name, password: password
      end
    end

    def self.create_using_generated_names_emails(n, password)
      n.times do |i|
        userX = "user#{i}"
        User.create! email: "#{userX}@dn#{i}", name: userX, password: password
      end
    end
end
