//= require rails-ujs
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require turbolinks
//= require_tree .

var ONE = 1;
var TWO = 2;

var timer;
var TIMER_DELAY_MS = 100;
var UNIQUE_CLASS_NAME;
var TIMER_EL_ID;

var timer_button_innerHTML;
var START = "Start";
var RESUME = "Resume";
var PAUSE = "Pause";

var HASH_TIMER_HYPHEN = "#timer-";
var TIMER_NOT_RUNNING_MESSAGE_ID_PART = "#timer-not-running-";
var TIMER_NOT_RUNNING_MESSAGE_ID;
var START_RESUME_TIMER = "Start/Resume timer!";
var HASH_TEAM_HYPHEN = "#team-";
var HYPHEN_USER_HYPHEN = "-user-";
var HYPHEN_SCORE_HYPHEN = "-score-";

function uniqueClassName(el) {
	if (UNIQUE_CLASS_NAME == undefined) {
		UNIQUE_CLASS_NAME = el.className;
		TIMER_NOT_RUNNING_MESSAGE_ID = TIMER_NOT_RUNNING_MESSAGE_ID_PART +
			UNIQUE_CLASS_NAME;
		TIMER_EL_ID = HASH_TIMER_HYPHEN + UNIQUE_CLASS_NAME;
	}
}

function timerNotRunning() {
	return timer_button_innerHTML == undefined ||
		timer_button_innerHTML === RESUME ||
		timer_button_innerHTML === START;
}

function timerRunning() {
	return timer_button_innerHTML === PAUSE;
}

function runTimer(this_el) {
	this_el.innerHTML = PAUSE;
	timer = setInterval(update_timer, TIMER_DELAY_MS);
	timer_button_innerHTML = PAUSE;
	$(TIMER_NOT_RUNNING_MESSAGE_ID).html("");
}

function stopTimer(this_el) {
	this_el.innerHTML = RESUME;
	clearInterval(timer);
	timer_button_innerHTML = RESUME;
}


// this function does not update the button inner html.
// (Now) It is used only after a team wins.
// If timer not started, it is not guaranteed what will happen.
function stopGameTimer() {
	clearInterval(timer)
}

function startOrPauseTimer(this_el) {
	uniqueClassName(this_el);
	timer_button_innerHTML = this_el.innerHTML;

	if (timerNotRunning()) runTimer(this_el);
	else if (timerRunning()) stopTimer(this_el);
}

function timerValueMS() {
	var timer_value = $(TIMER_EL_ID).html();
	return milliseconds(timer_value);
}

function timerValueS() {
	return Math.floor(timerValueMS() / 1000);
}

function update_timer() {
	var timer_ms = timerValueMS();
	var new_timer_value = get_new_timer_value(timer_ms + TIMER_DELAY_MS);
	$(TIMER_EL_ID).html(new_timer_value);
}

// assumes the generic form hh:mm:ss
// returns seconds
function get_seconds(t) {
	return Number(t.split(':')[0])*3600 + t.split(':')[1]*60 + Number(t.split(':')[2]);
}

// assumes the generic form hh:mm:ss:deciseconds
// returns milliseconds
function milliseconds(t) {
	return get_seconds(t)*1000 + Number(t.split(':')[3])*100;
}

// ms: milliseconds
function get_new_timer_value(ms) {
	var hours = Math.floor(ms / 3600000);
	var minutes = Math.floor(ms % 3600000 / 60000);
	var seconds = Math.floor(ms % 3600000 % 60000 / 1000);
	var milliseconds = ms % 3600000 % 60000 % 1000;
	var deciseconds = Math.floor(milliseconds/100);
	return hours + ":" + minutes + ":" + seconds + ":" + deciseconds;
}


// game data
var DURATION = 0;
var WIN_SCORE = 8;

var team1_won = false;
var team2_won = false;

var team1_user1_goals = 0;
var team1_user2_goals = 0;
var team2_user1_goals = 0;
var team2_user2_goals = 0;

var team1_user1_own_goals = 0;
var team1_user2_own_goals = 0;
var team2_user1_own_goals = 0;
var team2_user2_own_goals = 0;

var team1_user1_name;
var team1_user2_name;
var team2_user1_name;
var team2_user2_name;

var GAME_DATA;

// user_el_id - must have the form:
//	<%= team_no %>-<%= user_no %>-<%= @user11.id %>"
	function teamNo(user_el_id) {
		return Number(user_el_id.split('-')[0]);
	}

	function userNo(user_el_id) {
		return Number(user_el_id.split('-')[1]);
	}

function teamOneUserOneNotOwnGoals() {
	return team1_user1_goals - team1_user1_own_goals;
}

function teamOneUserTwoNotOwnGoals() {
	return team1_user2_goals - team1_user2_own_goals;
}

function teamOneNotOwnGoals() {
	return teamOneUserOneNotOwnGoals() + 
		teamOneUserTwoNotOwnGoals();
}

function teamOneOwnGoals() {
	return team1_user1_own_goals +
		team1_user2_own_goals;
}

function teamTwoUserOneNotOwnGoals() {
	return team2_user1_goals - team2_user1_own_goals;
}

function teamTwoUserTwoNotOwnGoals() {
	return team2_user2_goals - team2_user2_own_goals;
}

function teamTwoNotOwnGoals() {
	return teamTwoUserOneNotOwnGoals() + 
		teamTwoUserTwoNotOwnGoals();
}

function teamTwoOwnGoals() {
	return team2_user1_own_goals +
		team2_user2_own_goals;
}

function teamOneScore() {
	return teamOneNotOwnGoals() +
		teamTwoOwnGoals();
}

function teamTwoScore() {
	return teamTwoNotOwnGoals() +
		teamOneOwnGoals();
}

function teamOneWon() {
	team1_won = teamOneScore() == WIN_SCORE; 
	return team1_won;
}

function teamTwoWon() {
	team2_won = teamTwoScore() == WIN_SCORE; 
	return team2_won;
}

function aTeamWon() {
	return teamOneWon() || teamTwoWon();
}

// a team's id should have the form:
// "team-<%= team_no %>-score-<%= @user11.id %>", <%= where team_no %> is ONE or TWO
// and <%= @user11.id %> is UNIQUE_CLASS_NAME
	function updateTeamOneScore(team1Id) {
		$(team1Id).html( teamOneScore() );
	}

	function updateTeamTwoScore(team2Id) {
		$(team2Id).html( teamTwoScore() );
	}

	function updateTeamScores() {
		// I could move some variables (do it later)
		var hyphenScoreHyphenUniqueClassName = HYPHEN_SCORE_HYPHEN + UNIQUE_CLASS_NAME;
		var team1Id = HASH_TEAM_HYPHEN + ONE + hyphenScoreHyphenUniqueClassName;
		var team2Id = HASH_TEAM_HYPHEN + TWO + hyphenScoreHyphenUniqueClassName;
		updateTeamOneScore(team1Id);
		updateTeamTwoScore(team2Id);
	}

function prepareJSONObject() {
	var duration = "duration";
	var team1 = "team1";
	var team2 = "team2";
	var won = "won";
	var user1 = "user1";
	var user2 = "user2";
	var name = "name";
	var goals = "goals";
	var ownGoals = "ownGoals";
	
	GAME_DATA = {
		duration: DURATION,
		team1: {
			won: team1_won,
			user1: {
				name: team1_user1_name,
				goals: team1_user1_goals,
				ownGoals: team1_user1_own_goals
			},
			user2: {
				name: team1_user2_name,
				goals: team1_user2_goals,
				ownGoals: team1_user2_own_goals
			}
		},
		team2: {
			won: team2_won,
			user1: {
				name: team2_user1_name,
				goals: team2_user1_goals,
				ownGoals: team2_user1_own_goals
			},
			user2: {
				name: team2_user2_name,
				goals: team2_user2_goals,
				ownGoals: team2_user2_own_goals
			}
		}
	}
}

function sendGameData() {
	var S_GAME_DATA = JSON.stringify(GAME_DATA)
	$.ajax({
        url : "games/save",
        type : "post",
        data : { "game_data": S_GAME_DATA },
        success: document.body.innerHTML = ''
        // success: function() {
        // 	$.ajax({
        // 		url : "games/end",
        // 		type : "get",
        // 		data : { "winner1": "AAA", "winner2": "BBB"}
        // 	});
        // }
    });
}

function prepareGameData() {
	DURATION = timerValueS();
	team1_user1_name = $(HASH_TEAM_HYPHEN + ONE + HYPHEN_USER_HYPHEN + ONE + "-" + UNIQUE_CLASS_NAME).html();
	team1_user2_name = $(HASH_TEAM_HYPHEN + ONE + HYPHEN_USER_HYPHEN + TWO + "-" + UNIQUE_CLASS_NAME).html();
	team2_user1_name = $(HASH_TEAM_HYPHEN + TWO + HYPHEN_USER_HYPHEN + ONE + "-" + UNIQUE_CLASS_NAME).html();
	team2_user2_name = $(HASH_TEAM_HYPHEN + TWO + HYPHEN_USER_HYPHEN + TWO + "-" + UNIQUE_CLASS_NAME).html();
	prepareJSONObject();
}

function processGoal(user_el) {
	var user_el_id = user_el.id;
	var team_no = teamNo(user_el_id);
	var user_no = userNo(user_el_id);

	if (team_no == ONE) {
		if (user_no == ONE) team1_user1_goals += 1;
		else if (user_no == TWO) team1_user2_goals += 1;
	}
	else if (team_no == TWO) {
		if (user_no == ONE) team2_user1_goals += 1;
		else if (user_no == TWO) team2_user2_goals += 1;
	}

	if (aTeamWon()) {
		stopGameTimer();
		prepareGameData();
		sendGameData();
	}
	else updateTeamScores();
}

function processOwnGoal(user_el) {
	var user_el_id = user_el.id;
	var team_no = teamNo(user_el_id);
	var user_no = userNo(user_el_id);

	if (team_no == ONE) {
		if (user_no == ONE) {
			team1_user1_goals += 1;
			team1_user1_own_goals += 1;
		}
		else if (user_no == TWO) {
			team1_user2_goals += 1;
			team1_user2_own_goals += 1;
		}
	}
	else if (team_no == TWO) {
		if (user_no == ONE) { 
			team2_user1_goals += 1;
			team2_user1_own_goals += 1;
		}
		else if (user_no == TWO) {
			team2_user2_goals += 1;
			team2_user2_own_goals += 1;
		}
	}

	if (aTeamWon()) {
		stopGameTimer();
		prepareGameData();
		sendGameData();
	}
	else updateTeamScores();
}

function goal(this_user_el) {
	uniqueClassName(this_user_el);
	if (timerNotRunning()) $(TIMER_NOT_RUNNING_MESSAGE_ID).html(START_RESUME_TIMER);
	else if(timerRunning()) {
		processGoal(this_user_el);
	}
}

function ownGoal(this_user_el) {
	uniqueClassName(this_user_el);
	if (timerNotRunning()) $(TIMER_NOT_RUNNING_MESSAGE_ID).html(START_RESUME_TIMER);
	else if(timerRunning()) {
		processOwnGoal(this_user_el);
	}
}