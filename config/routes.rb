Rails.application.routes.draw do

  devise_for :users, :controllers => { registrations: 'registrations' }

  devise_scope :user do
    get 'login',  to: 'devise/sessions#new'
    get 'signup', to: 'devise/registrations#new'
    get 'edit',   to: 'devise/registrations#edit'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  match 'play(/:team1)(/:team2)(/:user11)(/:user12)(/:user21)(/:user22)' => 'games#play', as: :play, via: [:get, :post]
  post 'games/save' => 'games#save'
  get 'games/end' => 'games#end', as: :after_end_screen
  get 'readme'     => 'pages#readme'
  get 'statistics' => 'pages#stats', as: :statistics

  get 'statistics/own' => 'statistics#own', as: :own_statistics
  match 'statistics/all(/:name)' => 'statistics#all', as: :all_statistics, via: [:get, :post]

  root 'welcome#index'
end


