class CreateUserStatistics < ActiveRecord::Migration[5.1]
  def change
    create_table :user_statistics do |t|
      t.references :user, foreign_key: true
      t.integer :wins_no
      t.integer :games_played_no
      t.integer :goals_scored_no
      t.integer :own_goals_scored_no
      t.integer :time_played

      t.timestamps
    end
  end
end
