class Game < ApplicationRecord
  has_and_belongs_to_many :teams
  has_many :users, through: :teams

  validate :exact_two_teams, :distinct_teams, :distinct_users
	@WIN_SCORE = 8

	# validations
		def exact_two_teams
			if teams.size != 2
				errors.add(:"for game:", "not two teams in game")
			end
		end
		
		def distinct_teams
			team1 = teams.first
			team2 = teams.second
			if teams.size == 2 and team1 == team2
				errors.add(:"for game:", "game cannot have same team twice")
			end
		end

		def distinct_users
			user1 = teams.first.users.first
			user2 = teams.first.users.second
			user3 = teams.second.users.first
			user4 = teams.second.users.second

			# user1 and user2, and user3 and user4 are distinct because team users are distinct
			if user1 == user3 or user1 == user4 or user2 == user3 or user2 == user4
				errors.add(:"for game", "must have distinct users")
			end
		end

		def self.new_with(duration, team1, team2)
			new_game = Game.new duration: duration
      new_game.teams << team1 << team2 
      new_game.save!
		end
end
