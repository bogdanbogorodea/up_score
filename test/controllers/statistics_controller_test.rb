require 'test_helper'

class StatisticsControllerTest < ActionDispatch::IntegrationTest
  test "should get own" do
    get statistics_own_url
    assert_response :success
  end

  test "should get all" do
    get statistics_all_url
    assert_response :success
  end

end
