# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# a total of 200 users => a maximum of 19 900 teams
HOW_MANY_USERS_TO_BE_ADDED = 20
# hard coded format:
#   name: userX
#   email: userX@dnx
DEFAULT_PASSWORD = "p-1234"
# #1 Data for better testing
# trying to approximate: ~= 200 users, ~=10 min/game.
# 16 hours UP/day => 96 games/day => ~= 100 games/day.
# 3 years =>  ~=365 * 3 * 100 games = 1095 * 100 = 109 500 (games in 3 years)
# -> ~= 876 000 to 1 642 500 (goals in 3 years)
# 	=> test with 200 users, 210 000 games (-> 1 680 000 goals)
# Obs.: the teams no. and goals no. generation is influenced by games no.
# conclusion: enough to set only users to 200 and games to 210 000
# (HOW_MANY_USERS_TO_BE_ADDED and GAMES_NO)
# One problem: how much I should wait for seeding to end? If
# linear with GAMES_NO => around one hour

# #2 Ideas for efficiency improvement: make n:m between Game and Team, delete goals table and add two
# more tables for statistics updated automatically after each game has ended (after game is saved):
# 	#2.0 two tables user_statistics and team_statistics with fields: wins_no, games_played_no, goals_scored_no, own_goals_scored_no, time_played
# 		rails g model user_statistics user:references wins_no:integer games_played_no:integer goals_scored_no:integer own_goals_scored_no:integer time_played:integer
#			rails g model team_statistics team:references wins_no:integer games_played_no:integer goals_scored_no:integer own_goals_scored_no:integer time_played:integer
# 	#2.1 Add relation 1:1 between User and UserStatistic
#   	it will be faster to get users by number of wins and users by number of goals.
#   	it will be faster to get al time goaler with the number of goals
# 			rails g model user_statistics games_played_no:integer goals_scored_no:integer
# 			in User: has_one :statistic
# 	#2.2 Add relation 1:1 between Team and TeamStatistic
#   	=> popularity (equal to games_played_no), avg. time per game for a team
#   	=> teams by number of wins, teams by number of goals
#   	=> most popular team, least popular team

# I think goals table is useless. Better add a game_statistics table. The number of goals can be kept in the entity_nos table

# =>
# rails g model game duration:integer
# rails g model team
# rails g migration create_games_teams team:references game:references
# rails g migration create_teams_users user:references team:references
# rails g model user_statistics user:references wins_no:integer games_played_no:integer goals_scored_no:integer own_goals_scored_no:integer time_played:integer
# rails g model team_statistics team:references wins_no:integer games_played_no:integer goals_scored_no:integer own_goals_scored_no:integer time_played:integer

# user_statistics and team_statistics were recognized as a plurals and used in singular forms

# Game
  # has_and_belongs_to_many :teams
  # has_many :users, through: :teams

# Team
  # has_and_belongs_to_many :users
  # has_and_belongs_to_many :games
  # has_one :team_statistic

# User
  # has_and_belongs_to_many :teams
  # has_many :games, through: :teams
  # has_one :user_statistic

# TeamStatistic
#   belongs_to :team

# UserStatistic
#   belongs_to :user

# 300 users and 10000 games => 34m 31s seeding
# => >15 hours for 32 times more games
# => I will change the logic
#    (generate new teams faster - with users?)

# 8 users and 10000 games => 4m seeding
# 200 users and 210 000 games => h m s seding
# 200 users and 30 000 games => <6 hours

# games => teams and goals ( for a game will result two teams and 8 to 15 goals)


# other things to do:
	#1 autocomplete for game setup
	#2 autocomplete for selected user statistics
	#3 pagination for each ranking section (HTTP post for each page?) - 3-7 records
	#4 increase statistics pages efficiency (related to #3)
	#5 delete commentaries
	#6 increase seeding time efficiency - more tables with redundant data? Concurrency?
	#7
GAMES_NO = 200
# for game duration
DURATION_PER_GAME_INTERVAL = {
	left: 60,
	right: 7200
}

def random_boolean
	return false if Random.rand(2) == 0
	true
end

# P(true) = 0.9
def random_boolean_90_10
	return true if Random.rand(10) == 0
	false
end

# left < right
# returns in [left, right)
def random_in_interval(left, right)
	Random.rand(right - left) + left
end

def destroy_all
	UserStatistic.destroy_all
	TeamStatistic.destroy_all
	User.destroy_all
	Team.destroy_all
	Game.destroy_all
end

# return a team that has the two given users (if a team T from database already has the two users,
# return T, else create one with the two users)
def get_team(user1, user2, all_teams)
	all_teams.each do |team|
		users = team.users
		return [all_teams, team] if users.include? user1 and users.include? user2
	end
	new_team = Team.new_team(user1, user2)
	[all_teams << new_team , new_team]
end


# create n games
	def create_games_with_users(n, duration_interval, users, win_score = 8)
		all_teams = Team.all.to_a
		n.times do
			four_users = (users.sample 4).to_a
			duration = random_in_interval duration_interval[:left], duration_interval[:right]
			new_game = Game.new duration: duration

			team1_user1 = four_users.delete_at(Random.rand(four_users.size))
			team1_user2 = four_users.delete_at(Random.rand(four_users.size))
			team2_user1 = four_users.delete_at(Random.rand(four_users.size))
			team2_user2 = four_users.delete_at(Random.rand(four_users.size))

			# each of the returned teams may be new or already from the database
			teams_team = get_team team1_user1, team1_user2, all_teams
			all_teams = teams_team[0]
			team1 = teams_team[1]
			teams_team = get_team team2_user1, team2_user2, all_teams
			all_teams = teams_team[0]
			team2 = teams_team[1]

			new_game.teams << team1 << team2


			# play until one team wins (i.e. the game has ended)
			# players are users that play
			team1_users = team1.users
			team2_users = team2.users
			users_that_play = [].concat(team1_users).concat(team2_users)
			team1_user1_goals_scored = 0
			team1_user2_goals_scored = 0
			team2_user1_goals_scored = 0
			team2_user2_goals_scored = 0
			team1_user1_own_goals_scored = 0
			team1_user2_own_goals_scored = 0
			team2_user1_own_goals_scored = 0
			team2_user2_own_goals_scored = 0
			team1_score = 0
			team2_score = 0
			team1_won = false
			team2_won = false
			game_ended = false


			begin
				# own_goal? 10% chance
				own = random_boolean_90_10
				user_that_scored = (users_that_play.sample 1).first

				if team1_users.include? user_that_scored
					team1_user1_goals_scored += 1 if user_that_scored == team1_user1
					team1_user2_goals_scored += 1 if user_that_scored == team1_user2
					if own
						team1_user1_own_goals_scored += 1 if user_that_scored == team1_user1
						team1_user2_own_goals_scored += 1 if user_that_scored == team1_user2
						team2_score += 1
					else
						team1_score += 1
					end
				elsif team2_users.include? user_that_scored
					team2_user1_goals_scored += 1 if user_that_scored == team2_user1
					team2_user2_goals_scored += 1 if user_that_scored == team2_user2
					if own
						team2_user1_own_goals_scored += 1 if user_that_scored == team2_user1
						team2_user2_own_goals_scored += 1 if user_that_scored == team2_user2
						team1_score += 1
					else
						team2_score += 1
					end
				end

				if team1_score == win_score
					game_ended = team1_won = true
				elsif team2_score == win_score
					game_ended = team2_won = true
				end
			end until game_ended

			# update statistics
			# update statistics for team1 and its users
			team1_user1_statistic = team1_user1.user_statistic
			team1_user1_statistic.update_with team1_won, team1_user1_goals_scored, team1_user1_own_goals_scored, duration
			team1_user2_statistic = team1_user2.user_statistic
			team1_user2_statistic.update_with team1_won, team1_user2_goals_scored, team1_user2_own_goals_scored, duration
			team1_statistic = team1.team_statistic
			team1_goals_scored = team1_user1_goals_scored + team1_user2_goals_scored
			team1_own_goals_scored = team1_user1_own_goals_scored + team1_user2_own_goals_scored
			team1_statistic.update_with team1_won, team1_goals_scored, team1_own_goals_scored, duration

			# update statistics for team2 and its users
			team2_user1_statistic = team2_user1.user_statistic
			team2_user1_statistic.update_with team2_won, team2_user1_goals_scored, team2_user1_own_goals_scored, duration
			team2_user2_statistic = team2_user2.user_statistic
			team2_user2_statistic.update_with team2_won, team2_user2_goals_scored, team2_user2_own_goals_scored, duration
			team2_statistic = team2.team_statistic
			team2_goals_scored = team2_user1_goals_scored + team2_user2_goals_scored
			team2_own_goals_scored = team2_user1_own_goals_scored + team2_user2_own_goals_scored
			team2_statistic.update_with team2_won, team2_goals_scored, team2_own_goals_scored, duration

			new_game.save!
		end
	end



destroy_all

User.create_using_generated_names_emails HOW_MANY_USERS_TO_BE_ADDED, DEFAULT_PASSWORD
create_games_with_users GAMES_NO, DURATION_PER_GAME_INTERVAL, User.includes(:user_statistic).to_a
