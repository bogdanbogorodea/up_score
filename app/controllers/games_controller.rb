class GamesController < ApplicationController
	before_action :play_white_list_params, only: :play
  # before_action :congratulations_and_consolation_white_list_params,
  #   only: [:congratulations, :consolation]
  # before_action :in_white_list_params , only: :in
  protect_from_forgery with: :null_session
  # protect_from_forgery unless: -> { request.format.json? }
  before_action :update_database, only: :save

  attr_reader :team1_name,
    :team2_name,
    :user11, # team 1 user 1
    :user12, # team 1 user 2
    :user21, # ...
    :user22  # ...

  # for 'a just finished game'
  attr_reader :data,
    :duration,
    :team1_won,
    :team2_won,
    :u11, # user 1 of team 1
    :u12, # user 2 of team 1
    :u21, # ...
    :u22,
    :user11_goals_no,
    :user12_goals_no,
    :user21_goals_no,
    :user22_goals_no,
    :user11_own_goals_no,
    :user12_own_goals_no,
    :user21_own_goals_no,
    :user22_own_goals_no,
    :winner1,
    :winner2


  def play
    @new_game_errors_messages = []
    if request.post?
      analyze
      if no_errors
        render "in"
      end
    end
  end

  def save
    redirect_to after_end_screen_path(winner1: winner1, winner2: winner2)
  end

  def end
  end

  private
    def play_white_list_params
      if request.post?
        params.permit(:user11, :user12, :user21, :user22, :team1, :team2)
      end
    end

    def analyze_teams
      @team1_name = params[:team1]
      @team2_name = params[:team2]

      @new_game_errors_messages <<
        "team 1 has no name" if team1_name.empty?
      @new_game_errors_messages <<
        "team 2 has no name" if team2_name.empty?
      @new_game_errors_messages <<
        "teams have same name" if team1_name == team2_name
    end

    def analyze_users
      user11_name = params[:user11]
      user12_name = params[:user12]
      user21_name = params[:user21]
      user22_name = params[:user22]

      @new_game_errors_messages <<
        "user 1 of team 1 has no name" if user11_name.empty?
      @new_game_errors_messages <<
        "user 2 of team 1 has no name" if user12_name.empty?
      @new_game_errors_messages <<
        "user 1 of team 2 has no name" if user21_name.empty?
      @new_game_errors_messages <<
        "user 2 of team 2 has no name" if user22_name.empty?

      user_names = [] << user11_name << user12_name << user21_name << user22_name

      @new_game_errors_messages <<
        "some users have same name" if user_names.size != user_names.uniq.size

      @user11 = User.find_by name: params[:user11]
      @user12 = User.find_by name: params[:user12]
      @user21 = User.find_by name: params[:user21]
      @user22 = User.find_by name: params[:user22]

      @new_game_errors_messages <<
        "user \"#{user11}\" is not registered" if @user11.nil?
      @new_game_errors_messages <<
        "user \"#{user12}\" is not registered" if @user12.nil?
      @new_game_errors_messages <<
        "user \"#{user21}\" is not registered" if @user21.nil?
      @new_game_errors_messages <<
        "user \"#{user22}\" is not registered" if @user22.nil?
    end

    def analyze
      analyze_teams
      analyze_users
    end

    def no_errors
      @new_game_errors_messages.empty?
    end

    def set_game_data
      @data = JSON.parse(params[:game_data])
      @duration = data["duration"]
      team1 = data["team1"]
      team2 = data["team2"]

      user1 = "user1"
      user2 = "user2"
      won = "won"
      @team1_won = team1[won]
      @team2_won = team2[won]
      user11 = team1[user1]
      user12 = team1[user2]
      user21 = team2[user1]
      user22 = team2[user2]

      the_name = "name"
      goals = "goals"
      own_goals = "ownGoals"
      user11_name = user11[the_name]
      user12_name = user12[the_name]
      user21_name = user21[the_name]
      user22_name = user22[the_name]
      @u11 = User.find_by name: user11_name
      @u12 = User.find_by name: user12_name
      @u21 = User.find_by name: user21_name
      @u22 = User.find_by name: user22_name

      @user11_goals_no = user11[goals]
      @user12_goals_no = user12[goals]
      @user21_goals_no = user21[goals]
      @user22_goals_no = user22[goals]
      @user11_own_goals_no = user11[own_goals]
      @user12_own_goals_no = user12[own_goals]
      @user21_own_goals_no = user21[own_goals]
      @user22_own_goals_no = user22[own_goals]
    end

    def save_game_data
      all_teams = Team.includes(:users)
      team1 = Team.get_existing_or_new_team u11, u12, all_teams
      team2 = Team.get_existing_or_new_team u21, u22, all_teams

      u11.user_statistic.update_with team1_won,
        user11_goals_no,
        user11_own_goals_no,
        duration

      u12.user_statistic.update_with team1_won,
        user12_goals_no,
        user12_own_goals_no,
        duration

      u21.user_statistic.update_with team2_won,
        user21_goals_no,
        user21_own_goals_no,
        duration

      u22.user_statistic.update_with team2_won,
        user21_goals_no,
        user21_own_goals_no,
        duration

      team1.team_statistic.update_with team1_won,
        user11_goals_no + user12_goals_no,
        user11_own_goals_no + user12_own_goals_no,
        duration

      team2.team_statistic.update_with team2_won,
        user21_goals_no + user22_goals_no,
        user21_own_goals_no + user22_own_goals_no,
        duration

      Game.new_with duration, team1, team2

    end

    def set_winners
      if team1_won
        @winner1 = u11.name
        @winner2 = u12.name
      elsif team2_won
        @winner1 = u21.name
        @winner2 = u22.name
      end
    end

    def update_database
      set_game_data
      save_game_data
      set_winners
    end
end
