module StatisticsHelper
	# own statistics (use for all statistics too)
		# time_played: the total time user @user played
		# games_no: total number of games user @user played
		# goals_no: total number of goals user @user scored

		attr_reader :all_users, :all_games, :all_teams

		attr_reader :games_no, :goals_no

		def teams_from_db
			if @all_teams.nil?
				Team.includes(:team_statistic, :users)  # COME BACK!!!!!!!!!!!!!!! users are loaded two times! (one time here and one time above)
			else
				@all_teams
			end
		end

		# this data will be set for not doing some things more than once (e.g. execute @user.time_played)
		# how could i do this in a better way? (it should be bad to invoke this method from own
		# view - Could I use a callback instead?)
		def set_data_for_user
			@all_teams = teams_from_db
			user_statistic = @user.user_statistic
			@time_played = user_statistic.time_played
			@games_no = user_statistic.games_played_no
			@goals_no = user_statistic.goals_scored_no
		end

		# total time played by user @user in hours, minutes, seconds
		def time_played
			hours_minutes_seconds_from_seconds @time_played
		end

		# average time played per game in hours, minutes, seconds
		def avg_time
			average_time_per_game_in_seconds = divide @time_played, @games_no
			hours_minutes_seconds_from_seconds average_time_per_game_in_seconds
		end

		# average number of goals per game
		def avg_goals
			divide_and_round @goals_no, @games_no, 6
		end

				# three most frequent team members
		def three_most_frequent 
			@user.most_frequent_team_members 3, all_teams
		end



	# all statistics
		# selected user: no methods here, but the own statistics helper methods will be
		# invoked through user partial


		# Below, sum_no_wins_of_all_teams, sum_goals_no_of_all_teams, sum_wins_no_of_all_users,
		# sum_goals_no_of_all_users,sum_goals_no_of_all_users are not implemented methods. They are
		# just used to express tests.


		# teams_by_wins:
		# 	a test: sum_wins_no_of_all_teams == Game.count

		# teams_by_goals:
		#   a test: sum_goals_no_of_all_teams == sum_goals_no_of_all_users

		# users_by_wins:
		# 	a test: sum_wins_no_of_all_users == 2 * Game.count

		# users_by_goals:
		# 	a test: sum_goals_no_of_all_users == sum_goals_no_of_all_users
		attr_reader :teams_by_wins,
			:teams_by_goals,
			:users_by_wins,
			:users_by_goals,
			:all_time_goaler,
			:all_time_winner,
			:shortest_win_minutes,
			:longest_win_minutes,
			:most_popular_team,
			:least_popular_team,
			:teams_by_popularity

		# this data will be set for not doing some things more than once
		# how could i do this in a better way? (it should be bad to invoke
		# this method from all view - Could I use a callback instead?)
		# Should it be broken into more methods? I say yes, but first, I will try to increase efficiency.
		def set_data_for_all
			@all_teams = teams_from_db
			set_teams_by_wins_and_by_goals_and_popularity
			set_most_popular_team
			set_least_popular_team

			@all_users = User.includes(:user_statistic)
			set_users_by_wins_and_by_goals
			set_all_time_goaler
			set_all_time_winner

			@all_games = Game.all
			set_shortest_win_minutes
			set_longest_win_minutes
		end

		def set_teams_by_wins_and_by_goals_and_popularity
			@teams_by_wins = all_teams.order("team_statistics.wins_no DESC")
			@teams_by_goals = all_teams.order("team_statistics.goals_scored_no DESC")
			@teams_by_popularity = all_teams.order("team_statistics.games_played_no DESC")
		end

		def set_users_by_wins_and_by_goals
			@users_by_goals = all_users.order("user_statistics.goals_scored_no DESC")
			@users_by_wins = all_users.order("user_statistics.wins_no DESC")
		end

		def set_all_time_goaler
			@all_time_goaler = users_by_goals.first
		end

		def set_all_time_winner
			@all_time_winner = users_by_wins.first
		end

		def set_shortest_win_minutes
			@shortest_win_minutes = minutes_from_seconds all_games.minimum(:duration)
		end

		def set_longest_win_minutes
			@longest_win_minutes = minutes_from_seconds all_games.maximum(:duration)
		end
		
		def set_most_popular_team
			@most_popular_team = teams_by_popularity.last
		end

		def set_least_popular_team
			@least_popular_team = teams_by_popularity.first
		end

		def user?
			not @user.nil?
		end

		def user_name_not_received?
			@user_name_not_received
		end

		def user_name_errors?
			@user_name_errors
		end

		def user_name_error_messages
			@user_name_error_messages
		end


	# the below methods are more general applicability and used in more places in this class.
	# Should they be in StatisticsHelper?
  def divide(a, b)
    a.to_f/b.to_f
  end

  def modulo(a, b)
  	a.to_f % b.to_f
  end

  def divide_and_round(a, b, n=0)
    divide(a, b).round(n)
  end

  def modulo_and_round(a, b, n=0)
    modulo(a, b).round(n)
  end

  def divide_and_floor(a, b)
  	divide(a, b).floor
  end

  def modulo_and_floor(a, b)
  	modulo(a, b).floor
  end

  def minutes_from_seconds(seconds)
    divide_and_round seconds, 60
  end

  def hours_minutes_seconds_from_seconds(seconds)
    hours = divide_and_floor seconds, 3600
    mins = divide_and_floor seconds % 3600, 60
    secs = modulo_and_floor seconds % 3600, 60
    "#{hours} hour(s) #{mins} minute(s) #{secs} second(s)" 
  end

  def any_game_played?
    return false if Game.count == 0
    true
  end

end
