module GamesHelper
	CONGRATULATIONS_MESSAGES = [] << 
		"\"Winners never quit and quitters never win.\" - Vince Lombardi" <<
		"\"It's about you. If you win, it's you; if you lose, it's you. Black and white. Nowhere to hide.\" - Greg Rusedski" <<
		"\"Win or lose, I'll feel good about myself. That's what is important.\" - Mary Docter" <<
		"That was a great game! You totally rocked!" <<
		"To more victories! Cheers and congratulations!" <<
		"Your team play was the best!" <<
		"Congratulations! I bet you are on cloud nine right now." <<
		"Congratulations to us!" <<
		"We are the champions! Congratulations to us, team!" <<
		"You ripped your opponent to pieces." <<
		"You ripped your opponent to pieces. That’s my boy!" <<
		"Congratulations! That’s my boy!" <<
		"Congratulations! You ripped your opponent to pieces. That’s my boy!" <<
		"Epic win!" <<
		"You totally owned them!" <<
		"Legendary!" <<
		"Godlike!" <<
		"Unstoppable!" <<
		"Dominating!"

	CONSOLATION_MESSAGES = [] <<
		"\"I would prefer even to fail with honor than win by cheating.\" - Sophocles" <<
		"\"Sometimes by losing a battle you find a new way to win the war.\" - Donald Trump" <<
		"\"First they ignore you, then they laugh at you, then they fight you, then you win.\" - Mahatma Gandhi" <<
		"\"Winners never quit and quitters never win.\" - Vince Lombardi" <<
		"\"You were born to win, but to be a winner, you must plan to win, prepare to win, and expect to win.\" - Zig Ziglar" <<
		"\"You can't win unless you learn how to lose.\" - Kareem Abdul-Jabbar" <<
		"\"It's about you. If you win, it's you; if you lose, it's you. Black and white. Nowhere to hide.\" - Greg Rusedski" <<
		"\"Win or lose, I'll feel good about myself. That's what is important.\" - Mary Docter" <<
		"\"I want to have fun. It's a beautiful life. You learn, you win, you lose, but you get up.\" - Nas" <<
		"\"Victorious warriors win first and then go to war, while defeated warriors go to war first and then seek to win.\" - Sun Tzu" <<
		"Well done, team. Congratulations for winning second place. The best is yet to come."

	def congratulations
		CONGRATULATIONS_MESSAGES[Random.rand(CONGRATULATIONS_MESSAGES.size)]
	end

	def consolation
		CONSOLATION_MESSAGES[Random.rand(CONSOLATION_MESSAGES.size)]
	end

	def errors
	  not @new_game_errors_messages.empty?
	end

	def new_game_errors_messages
		@new_game_errors_messages
	end
end